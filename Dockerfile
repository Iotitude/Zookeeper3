FROM java:8
# Install required software
RUN apt-get update && apt-get install -y \
  net-tools wget
# Create and use as workdir /usr/src directory
RUN mkdir -p /usr/src/
WORKDIR /usr/src
# Install Zookeeper version 3.4.8
RUN wget http://mirror.netinch.com/pub/apache/zookeeper/zookeeper-3.4.8/zookeeper-3.4.8.tar.gz
RUN tar -xzvf zookeeper-3.4.8.tar.gz
RUN mv /usr/src/zookeeper-3.4.8/conf/zoo_sample.cfg /usr/src/zookeeper-3.4.8/conf/zoo.cfg
# Open Port 2181
EXPOSE 2181
# Run commands
ENTRYPOINT ["./zookeeper-3.4.8/bin/zkServer.sh"]
CMD ["start-foreground"]